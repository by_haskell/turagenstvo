<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 0:30
 */
?>

<?php include_once 'include/selection_tour.php'; ?>

<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<link rel="icon" href="/favicon.ico">

	<title>Формирование заказа - Darkill</title>

	<!-- Bootstrap core CSS -->
	<link href="/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
    <link href="/css/form-validation.css" rel="stylesheet">
    <link href="/css/product.css" rel="stylesheet">
</head>

<body class="bg-light">

<div class="container">
	<div class="py-5 text-center">
		<img class="d-block mx-auto mb-4" src="/img/globe.svg" alt="" width="72" height="72">
		<h2>Турагенство - Darkill</h2>
		<p class="lead">Теперь нужно определиться в какой тур, Вы бы хотели отправится.</p>
		<p class="lead">Все поля должны быть заполнены обязательно!</p>
	</div>

	<div class="row">
		<div class="col-md-4 order-md-2 mb-4">
			<h4 class="d-flex justify-content-between align-items-center mb-3">
				<span class="text-muted">Туры по странам</span>
				<span class="badge badge-secondary badge-pill"><?php include 'include/count_tour_country.php'; ?></span>
			</h4>
			<ul class="list-group mb-3">
                <?php include 'include/tour_country.php'; ?>
			</ul>
		</div>
		<div class="col-md-8 order-md-1">
			<h4 class="mb-3">Оформление отдыха</h4>
			<form class="needs-validation" novalidate="" method="post" action="design_of_the_tour.php">
				<div class="row">
					<div class="col-md-6 mb-3">
						<label for="firstName">Фамилия</label>
						<input type="text" class="form-control" id="firstName" name="firstName" placeholder="" value="" required="">
						<div class="invalid-feedback">
							Это поле обязательно для заполнения.
						</div>
					</div>
					<div class="col-md-6 mb-3">
						<label for="lastName">Имя</label>
						<input type="text" class="form-control" id="lastName" name="lastName" placeholder="" value="" required="">
						<div class="invalid-feedback">
                            Это поле обязательно для заполнения.
						</div>
					</div>
				</div>

				<div class="mb-3">
					<label for="phone">Номер телефона</label>
					<input type="tel" class="form-control" id="phone" name="phone" required="" placeholder="+78949493929">
					<div class="invalid-feedback">
                        Это поле обязательно для заполнения.
					</div>
				</div>

                <div class="mb-3">
                    <label for="phone">Дата рождения</label>
                    <input type="date" class="form-control" id="date" required="" name="date">
                    <div class="invalid-feedback">
                        Это поле обязательно для заполнения.
                    </div>
                </div>

                <div class="mb-3">
                    <label for="tour">Выберите тур</label>
                    <?=$html?>
                    <input type="text" class="form-control tour_selected" id="tour" name="tour" required="">
                    <div class="invalid-feedback tour-selected">
                        Это поле обязательно для заполнения.
                    </div>
				</div>

				<hr class="mb-4">
				<button class="btn btn-primary btn-lg btn-block send_applications" type="submit">Оформить заявку</button>
			</form>
		</div>
	</div>

	<footer class="my-5 pt-5 text-muted text-center text-small">
		<p class="mb-1">Турагенство - Darkill &copy; <?=date('Y')?></p>
	</footer>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="/js/vendor/popper.min.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/vendor/holder.min.js"></script>
<script src="/js/custom.js"></script>
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';

        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');

            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();
</script>
</body>
</html>