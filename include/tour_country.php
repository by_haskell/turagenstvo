<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 1:13
 */
	//Соединяемся с базой
	spl_autoload_register(function ($classname) {
		require  $classname . '.php';
	});

	$obj = new database();
	$pdo = $obj->getDatabase();
	$pdo = $obj->getDatabaseError();

	//Проверяем, есть ли вообще страны в базе
	$query_country = $pdo->query("SELECT COUNT(*) as count FROM `country`");
	$query_country->setFetchMode(PDO::FETCH_ASSOC);
	$count_country = $query_country->fetch();
	if ($count_country['count'] > 0) {
		//Формируем массив данных со странами мира которые есть в базе
		$array_country = $pdo->prepare("SELECT * FROM `country` LIMIT 3");
		$array_country->execute();
		while($country = $array_country->fetch(PDO::FETCH_ASSOC)){
			//Так как файл инклудится при выводе прямо в select, то нам нужно передать лишь option.
			echo '<li class="list-group-item d-flex justify-content-between lh-condensed">
					<div class="text-center">
						<div class="my-0">
						  <img class="img-country-tour" src="/img/country/'.$country['images'].'" class="rounded" alt="'.$country['name'].'">
						</div>
						<small class="text-muted">'.$country['name'].'</small>
					</div>
				</li>';
		}
	}