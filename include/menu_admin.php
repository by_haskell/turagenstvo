<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 21:09
 */
?>
<ul class="nav" id="side-menu">
	<li>
		<a href="/admin"><i class="fa fa-dashboard fa-fw"></i>Панель</a>
	</li>
	<li>
		<a href="#"><i class="fa fa-group fa-fw"></i> Клиенты<span class="fa arrow"></span></a>
		<ul class="nav nav-second-level">
			<li>
				<a href="/admin/client">Список</a>
			</li>
		</ul>
		<!-- /.nav-second-level -->
	</li>
	<li>
		<a href="#"><i class="fa fa-home fa-fw"></i> Страны<span class="fa arrow"></span></a>
		<ul class="nav nav-second-level">
			<li>
				<a href="/admin/country">Список</a>
			</li>
			<li>
				<a href="/admin/country/new.php">Добавить новую</a>
			</li>
		</ul>
		<!-- /.nav-second-level -->
	</li>
	<li>
		<a href="#"><i class="fa fa-plane fa-fw"></i> Туры<span class="fa arrow"></span></a>
		<ul class="nav nav-second-level">
			<li>
				<a href="/admin/tour">Список</a>
			</li>
			<li>
				<a href="/admin/tour/new.php">Добавить новый</a>
			</li>
		</ul>
		<!-- /.nav-second-level -->
	</li>
	<li>
		<a href="#"><i class="fa fa-eye fa-fw"></i> Заявки<span class="fa arrow"></span></a>
		<ul class="nav nav-second-level">
			<li>
				<a href="/admin/request">Список неразобранных</a>
			</li>
			<li>
				<a href="/admin/request/all">Полный список</a>
			</li>
		</ul>
		<!-- /.nav-second-level -->
	</li>
</ul>
