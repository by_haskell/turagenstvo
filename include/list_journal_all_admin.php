<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 0:20
 */
include_once 'setting.php';

if($_SESSION['login'] == $login or $_SESSION['password'] == $password) {

	//Соединяемся с базой
	spl_autoload_register( function ( $classname ) {
		require '../../../' . $classname . '.php';
	} );

	$obj = new database();
	$pdo = $obj->getDatabase();
	$pdo = $obj->getDatabaseError();

	//Проверяем, есть ли вообще страны в базе
	$query_journal = $pdo->query( "SELECT COUNT(*) as count FROM `journal`" );
	$query_journal->setFetchMode( PDO::FETCH_ASSOC );
	$count_journal = $query_journal->fetch();
	if ( $count_journal['count'] > 0 ) {
		//Формируем массив данных со странами мира которые есть в базе
		$array_journal = $pdo->prepare( "SELECT * FROM `journal`" );
		$array_journal->execute();
		while ( $journal = $array_journal->fetch( PDO::FETCH_ASSOC ) ) {
			$array_tour = $pdo->prepare( "SELECT * FROM `tour` WHERE id='$journal[tour_id]'" );
			$array_tour->execute();
			$tour         = $array_tour->fetch( PDO::FETCH_ASSOC );
			$array_client = $pdo->prepare( "SELECT * FROM `client` WHERE id='$journal[client_id]'" );
			$array_client->execute();
			$client = $array_client->fetch( PDO::FETCH_ASSOC );
			echo '<tr class="gradeA odd" role="row">
						<td class="sorting_1">' . $journal['id'] . '</td>
						<td class="sorting_1">' . $tour['name'] . '</td>
						<td class="sorting_1">' . $client['name'] . '</td>
						<td class="sorting_1">';
			if ( $journal['action'] == 2 ) {
				echo 'Подтвержден';
			} elseif ( $journal['action'] == 1 ) {
				echo 'Закрыта';
			} else {
				echo 'Ожидает обработки';
			}
			echo '</td>
						<td class="sorting_1">' . date( 'l, d F Y', $journal['time'] ) . '</td>
					</tr>';
		}
	}
}