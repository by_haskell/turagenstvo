<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 20:00
 */
session_start();
//Подгружаем данные администратора
include_once 'setting.php';
//Проверяем сесию, в котором записывается данные админа для авторизации, если пусты данные - то ставим метку что нужно вернуть пользователя
//на страницу авторизации
if(empty($_SESSION['login']) and empty($_SESSION['password'])) {
	$auth = false;
}
//Проверяем, если есть какие то данные
//они должны совпадают данные в сессии пользователя и с настройками доступа
if($_SESSION['login'] != $login or $_SESSION['password'] != $password) {
	$auth = false;
}else{
	$auth = true;
}
//Проверяем, если $auth равен true, то пользователю можно продолжать быть на странице
//Если нет, то переадресовываем на страницу авторизации
if(!$auth) {?>
	<meta http-equiv="refresh" content="0;url=/admin/login.php">
	<script language="javascript">
        window.location.href = "/admin/login.php"
	</script>
<?php }