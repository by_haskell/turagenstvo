-- phpMyAdmin SQL Dump
-- version 4.7.7
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Час створення: Квт 22 2018 р., 04:00
-- Версія сервера: 5.6.38
-- Версія PHP: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База даних: `turagent`
--

-- --------------------------------------------------------

--
-- Структура таблиці `client`
--

CREATE TABLE `client` (
  `id` int(10) NOT NULL,
  `name` varchar(255) NOT NULL COMMENT 'ФИО клиента',
  `phone` varchar(15) NOT NULL COMMENT 'Номер телефона',
  `date_of_birth` varchar(50) NOT NULL COMMENT 'Дата рождения'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `client`
--

INSERT INTO `client` (`id`, `name`, `phone`, `date_of_birth`) VALUES
(1, 'Александр Курочка', '961493055', '1994-12-24'),
(2, 'Новикова К.П.', '+79376574545', ''),
(3, 'Кузьмин М.Х.', '+79064575634', ''),
(4, 'Антонов И.Е.', '+89463334565', ''),
(5, 'Антонова Л.В.', '+89463334565', ''),
(6, 'Антонова А.И.', '+89463334565', ''),
(7, 'Test Testovich', '+3123123123213', '2010-12-24');

-- --------------------------------------------------------

--
-- Структура таблиці `country`
--

CREATE TABLE `country` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `images` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `country`
--

INSERT INTO `country` (`id`, `name`, `images`) VALUES
(1, 'Германия', '1477310733_06_germany_normal.jpg'),
(2, 'Япония', 'japan_mt_fuji.jpg'),
(3, 'Франция', '2017-08-06t162822z_733073559_rc1b963f3a10_rtrmadp_3_france-attack-eiffeltower.jpg'),
(4, 'Австралия', 'australia.jpg'),
(5, 'Малдивы', 'maldivi.jpg'),
(6, 'Шри-Ланка', 'shrilanka.jpg'),
(7, 'Таиланд', 'tailand.jpg'),
(8, 'Турция', 'TUR.jpg'),
(9, 'Кипр', '331-8818c53dd8502daa049d91522b563759litva-latviya-1024x576.jpg'),
(10, 'Литва', '331-8818c53dd8502daa049d91522b563759litva-latviya-1024x576.jpg'),
(11, 'Россия', '457-8818c53dd8502daa049d91522b563759litva-latviya-1024x576.jpg');

-- --------------------------------------------------------

--
-- Структура таблиці `journal`
--

CREATE TABLE `journal` (
  `id` int(11) NOT NULL,
  `tour_id` int(11) NOT NULL COMMENT 'ID тура с таблицы tour',
  `client_id` int(11) NOT NULL COMMENT 'ID клиента с таблицы ckient',
  `date` varchar(20) NOT NULL COMMENT 'Unix метка времени',
  `action` int(1) NOT NULL COMMENT 'Статус заявки. 1 - одобрена, 2 - отклонена, 0 - не рассмотрена'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `journal`
--

INSERT INTO `journal` (`id`, `tour_id`, `client_id`, `date`, `action`) VALUES
(1, 1, 3, '1002020202', 2),
(2, 1, 4, '1002020202', 2),
(3, 1, 5, '1002020202', 0),
(4, 2, 1, '1002020202', 1),
(5, 2, 2, '1002020202', 0),
(6, 1, 0, '1524266959', 0),
(7, 1, 7, '1524356988', 2);

-- --------------------------------------------------------

--
-- Структура таблиці `tour`
--

CREATE TABLE `tour` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `country` int(11) NOT NULL,
  `description` text NOT NULL,
  `images` varchar(100) NOT NULL,
  `action` int(1) NOT NULL,
  `time` varchar(20) NOT NULL,
  `price` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп даних таблиці `tour`
--

INSERT INTO `tour` (`id`, `name`, `country`, `description`, `images`, `action`, `time`, `price`) VALUES
(1, 'Шедевры классической Японии под красными клёнами', 2, 'Описание шедевров', '9.jpg', 0, '1212322333', '900'),
(2, 'Пробежка по винным погребам Франции', 1, 'Описание пробежки', '9.jpg', 0, '1212322333', '950'),
(3, 'Шедевры классической Японии под красными клёнами', 2, 'Описание шедевров', '9.jpg', 0, '1212322333', '1000'),
(4, 'На берегах Литвы', 10, 'Вкратце: неплохо)', '714-8818c53dd8502daa049d91522b563759litva-latviya-1024x576.jpg', 0, '1252322333', '1050'),
(5, 'Москва', 11, 'Осмотр Москвы и ее окрепности.', '185-7a19f569790d075388805436af69a8f6Russia.jpg', 0, '1524357298', '1200'),
(6, 'Ростов', 11, 'Путешествие по Ростов-на-Дону', '497-840285f1a69d428555b994865126ec62лицо-10.jpg', 0, '1524357782', '1500');

--
-- Індекси збережених таблиць
--

--
-- Індекси таблиці `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `country`
--
ALTER TABLE `country`
  ADD PRIMARY KEY (`id`),
  ADD KEY `ID` (`id`);

--
-- Індекси таблиці `journal`
--
ALTER TABLE `journal`
  ADD PRIMARY KEY (`id`);

--
-- Індекси таблиці `tour`
--
ALTER TABLE `tour`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для збережених таблиць
--

--
-- AUTO_INCREMENT для таблиці `client`
--
ALTER TABLE `client`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблиці `country`
--
ALTER TABLE `country`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT для таблиці `journal`
--
ALTER TABLE `journal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT для таблиці `tour`
--
ALTER TABLE `tour`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
