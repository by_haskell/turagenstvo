<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 0:34
 */
	//Объявляем переменную в которой будет хранится весь html код выводимый на странице оформления
	$html = '';
	//Соединяемся с базой
	spl_autoload_register(function ($classname) {
		require  $classname . '.php';
	});

	$obj = new database();
	$pdo = $obj->getDatabase();
	$pdo = $obj->getDatabaseError();

	//Проверяем, есть ли вообще тур для этой страны в базе
	$query_tour = $pdo->query("SELECT COUNT(*) as count FROM `tour`");
	$query_tour->setFetchMode(PDO::FETCH_ASSOC);
	$count_tour = $query_tour->fetch();
	if ($count_tour['count'] > 0) {

		//Формируем массив данных с турами страны которые есть в базе
		$array_tour = $pdo->prepare("SELECT * FROM `tour`");
		$array_tour->execute();
		while($tour = $array_tour->fetch(PDO::FETCH_ASSOC)){
			$array_country = $pdo->prepare("SELECT * FROM `country` WHERE id='$tour[country]'");
			$array_country->execute();
			$country = $array_country->fetch(PDO::FETCH_ASSOC);
			//Формируем тело html который выведется пользователю
			$html .= '
                <div class="col-md-4">
                    <div class="card mb-4 box-shadow">
                        <img class="card-img-top" src="/img/tour/'.$tour['images'].'" data-src="/img/tour/'.$tour['images'].'" alt="'.$tour['name'].'">
                        <div class="card-body">
                            <p class="card-text">'.$tour['description'].'</p>
                            <p class="card-text"><small class="text-muted lead">Стоимость: '.$tour['price'].' ₽</small></p>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-sm btn-outline-secondary">'.$country['name'].'</button>
                                </div>
                                <small class="text-muted">Размещено: '.date('l, d F Y', $tour['time']).'</small>
                            </div>
                        </div>
                    </div>
                </div>';
		}
	}else{
		$html .='<div class="alert alert-danger" role="alert">
		  <h4 class="alert-heading">Внимание!</h4>
		  <p>В выбранной Вами стране наше турагенство еще не предоставляет услуги.</p>
		  <hr>
		  <p class="mb-0">Вернуться к <a href="/country_of_rest.php">выбору страны</a>.</p>
		</div>';
	}
