<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 0:20
 */

include_once 'setting.php';

if($_SESSION['login'] == $login or $_SESSION['password'] == $password) {
	//Соединяемся с базой
	spl_autoload_register( function ( $classname ) {
		require '../../' . $classname . '.php';
	} );

	$obj = new database();
	$pdo = $obj->getDatabase();
	$pdo = $obj->getDatabaseError();

	//Проверяем, есть ли вообще страны в базе
	$query_tour = $pdo->query( "SELECT COUNT(*) as count FROM `tour`" );
	$query_tour->setFetchMode( PDO::FETCH_ASSOC );
	$count_tour = $query_tour->fetch();
	if ( $count_tour['count'] > 0 ) {
		//Формируем массив данных со странами мира которые есть в базе
		$array_tour = $pdo->prepare( "SELECT * FROM `tour`" );
		$array_tour->execute();
		while ( $tour = $array_tour->fetch( PDO::FETCH_ASSOC ) ) {
			$array_country = $pdo->prepare( "SELECT * FROM `country` WHERE id='$tour[country]'" );
			$array_country->execute();
			$country = $array_country->fetch( PDO::FETCH_ASSOC );
			echo '<tr class="gradeA odd" role="row">
						<td class="sorting_1">' . $tour['name'] . '</td>
						<td class="sorting_1">' . $country['name'] . '</td>
						<td class="sorting_1">' . $tour['description'] . '</td>
						<td class="sorting_1">';
			if ( $tour['action'] == 0 ) {
				echo 'Актуален';
			} else {
				echo 'Закрыт';
			}
			echo '</td>
						<td class="sorting_1">' . date( 'l, d F Y', $tour['time'] ) . '</td>
						<td><a href="/img/tour/' . $tour['images'] . '">
							<div class="text-center">
                                <img src="/img/tour/' . $tour['images'] . '" class="rounded img-tour" alt="' . $tour['name'] . '">
							</div></a>
						</td>
					</tr>';
		}
	}
}