<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 20.04.2018
 * Time: 23:49
 */
?>
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>Выбор страны для своего отдыха - Darkill</title>

    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/form-validation.css" rel="stylesheet">
  </head>

  <body class="bg-light">

    <div class="container">
      <div class="py-5 text-center">
        <img class="d-block mx-auto mb-4" src="/img/globe.svg" alt="" width="72" height="72">
        <h2>Турагенство - Darkill</h2>
        <p class="lead">Для выбора наилучшего предложения для Вас, выберите пожалуйста страну в которой бы хотели отдохнуть.</p>
      </div>

      <div class="row">
        <div class="col-md-12 order-md-2 mb-4">
	        <form class="form-inline" method="post" action="choice_tour.php">
		        <label class="my-1 mr-2" for="inlineFormCustomSelectPref">Страна для отдыха</label>
		        <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref" name="country">
			        <option selected>Выбрать...</option>
			        <?php include 'include/list_country.php'; ?>
		        </select>

		        <button type="submit" class="btn btn-primary my-1">Следующий этап</button>
	        </form>
        </div>
      </div>

      <footer class="my-5 pt-5 text-muted text-center text-small">
        <p class="mb-1">Турагенство - Darkill &copy; <?=date('Y')?></p>
      </footer>
    </div>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="/js/vendor/popper.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/vendor/holder.min.js"></script>
    <script>
      // Example starter JavaScript for disabling form submissions if there are invalid fields
      (function() {
        'use strict';

        window.addEventListener('load', function() {
          // Fetch all the forms we want to apply custom Bootstrap validation styles to
          var forms = document.getElementsByClassName('needs-validation');

          // Loop over them and prevent submission
          var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
              if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
              }
              form.classList.add('was-validated');
            }, false);
          });
        }, false);
      })();
    </script>
  </body>
</html>
