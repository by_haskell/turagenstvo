$(document).ready(function () {
    $("body").on('click', '.card', function () {
        $(".card").removeClass('selected_tour').css('opacity', '0.7');
        $(this).addClass('selected_tour').css('opacity', '1');
        $('.tour_selected').val(this.id);
    });
});