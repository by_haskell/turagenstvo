<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 20:18
 */
//Подгружаем данные администратора
include_once 'setting.php';
//Переменная для проведения процесса авторизации
$check_auth = false;
//Проверка не зашел ли на страницу авторизированный администратор
//Проверка на пустоту сесии
if(!empty($_SESSION['login']) and !empty($_SESSION['password'])){
	//Если сессия не пуста, то проверяем данные
	if($_SESSION['login'] == $login and $_SESSION['password'] == $password){
		//К нам зашел авторизированный пользователь, потому перенаправляем на главную страницу админки
		?>
		<meta http-equiv="refresh" content="0;url=index.php">
		<script language="javascript">
            window.location.href = "index.php"
		</script>
		<?php
	}
}
//Проверяем есть ли массив данных от пользователя для проверки
if(isset($_POST['submit'])){
	//Проверяем все ли поля заполнены для проверки
	if(!empty($_POST['login']) and !empty($_POST['password'])){
		//Сверяем данные
		//md5 - алгоритм шифрования паролей в php
		//Хранить пароль без шифрования нельзя в целях безопастности кражи сессии
		if($_POST['login'] == $login and (md5($_POST['password']) == $password)){
			//Ставим метку что пользователь все верно ввел
			$check_auth = true;
		}
	}
}
//Проверка статуса метки определителя авторизации
if($check_auth){
	//Записываем в сессию
	$_SESSION['login'] = $_POST['login'];
	//Шифруем алгоритмом md5, хранить в сессии не зашифрованным пароль нельзя!!!
	$_SESSION['password'] = md5($_POST['password']);
	//Переадресовываем на главную страницу админки
	?>
	<meta http-equiv="refresh" content="0;url=index.php">
	<script language="javascript">
        window.location.href = "index.php"
	</script>
<?php }else{
	//Авторизация не прошла
	//Что бы пользователь меньше думал что ввел не верно, выдаем ему точные ошибки

	//Если данные вводились то submit должен существовать иначе пользователь еще ничего не ввел, а потому вывод ошибок не нужен
	if(isset($_POST['submit'])) {
		//Записываем данные которые были введены пользователем, которые выведем на странице что бы еще раз не заполнял
		$_SESSION['form_login'] = $_POST['login'];
		$_SESSION['form_password'] = $_POST['password'];
		//Проверки на пустоту
		if ( empty( $_POST['login'] ) ) {
			echo '<div class="alert alert-danger" role="alert">
		  <h4 class="alert-heading">Ошибка авторизации!</h4>
		  <p>Извините, но для авторизации нужно заполнить логин.</p>
		  <p class="mb-0">Попробуйте еще раз, пожалуйста.</p>
		</div>';
		} elseif ( empty( $_POST['password'] ) ) {
			echo '<div class="alert alert-danger" role="alert">
		  <h4 class="alert-heading">Ошибка авторизации!</h4>
		  <p>Извините, но для авторизации нужно заполнить пароль.</p>
		  <p class="mb-0">Попробуйте еще раз, пожалуйста.</p>
		</div>';
			//Проверки на совпадение
		} elseif ( $_POST['login'] != $login ) {
			echo '<div class="alert alert-danger" role="alert">
		  <h4 class="alert-heading">Ошибка авторизации!</h4>
		  <p>Извините, но введенный Вами логин не верный.</p>
		  <p class="mb-0">Попробуйте еще раз, пожалуйста.</p>
		</div>';
		} elseif ( $_POST['password'] != $password ) {
			echo '<div class="alert alert-danger" role="alert">
		  <h4 class="alert-heading">Ошибка авторизации!</h4>
		  <p>Извините, но введенный Вами пароль не верный.</p>
		  <p class="mb-0">Попробуйте еще раз, пожалуйста.</p>
		</div>';
		}
	}
}
