<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 2:07
 */

include_once 'setting.php';

if($_SESSION['login'] == $login or $_SESSION['password'] == $password) {
	//Соединяемся с базой
	spl_autoload_register( function ( $classname ) {
		require $classname . '.php';
	} );

	$obj = new database();
	$pdo = $obj->getDatabase();
	$pdo = $obj->getDatabaseError();
	//Объявляем переменную в которой будет хранится весь html код выводимый на странице оформления
	$html = '';
	//Проверяем что бы к нам пришел не пустой массив данных. Проверка на пустой массив должна быть отрицательной
	if ( ! empty( $_POST ) ) {
		//Проверяем не пустые ли данные к нам пришли
		if (
			! empty( $_POST['firstName'] ) and
			! empty( $_POST['lastName'] ) and
			! empty( $_POST['phone'] ) and
			! empty( $_POST['tour'] ) and
			! empty( $_POST['date'] )
		) {
			$firstName = trim( $_POST['firstName'] );
			$lastName  = trim( $_POST['lastName'] );
			$phone     = trim( $_POST['phone'] );
			$tour      = trim( $_POST['tour'] );
			$date      = trim( $_POST['date'] );

			//Заносим новую заявку в базу для обработки
			//Изначально заносим клиента
			$array_client['name']          = $firstName . ' ' . $lastName;
			$array_client['phone']         = $phone;
			$array_client['date_of_birth'] = $date;

			$insert_client = $pdo->prepare( "INSERT INTO `client` (name, phone, date_of_birth) values (:name, :phone, :date_of_birth)" );
			$insert_client->execute( $array_client );

			//Узнаем ID клиента
			$id = $pdo->lastInsertId();

			//Теперь заносим заявку
			$array_journal['tour_id']   = $tour;
			$array_journal['client_id'] = $id;
			$array_journal['date']      = time();

			$insert_journal = $pdo->prepare( "INSERT INTO `journal` (tour_id, client_id, date) values (:tour_id, :client_id, :date)" );
			$insert_journal->execute( $array_journal );

			//Готовим сообщение об успешной отправке на обработку заявку
			$html .= '<div class="alert alert-success" role="alert">
				  <h4 class="alert-heading">Заявка оформлена!</h4>
				  <p>Спасибо за доверие к нашему турагенству. Менеджер в течении часа-двух созвонится с Вами.</p>
				  <hr>
				  <p class="mb-0">Вернуться на <a href="/">главную</a>.</p>
				</div>';
		} else {
			$html .= '<div class="alert alert-danger" role="alert">
				  <h4 class="alert-heading">Ошибка обработки!</h4>
				  <p>Предоставленные данные для анализа и подбора соответствующего тура были искажены. Попробуйте заново.</p>
				  <hr>
				  <p class="mb-0">Вернуться к <a href="/country_of_rest.php">выбору страны</a>.</p>
				</div>';
		}
	} else {
		$html .= '<div class="alert alert-danger" role="alert">
				  <h4 class="alert-heading">Ошибка обработки!</h4>
				  <p>Вы не предоставили нам никаких данных для анализа и подбора соответствующего тура.</p>
				  <hr>
				  <p class="mb-0">Вернуться к <a href="/country_of_rest.php">выбору страны</a>.</p>
				</div>';
	}
}