<?php
class Database {

    private $host   = '127.0.0.1';
    private $dbname = 'turagent';
    private $user   = 'root';
    private $pass   = '';

    private $db     = null;

    function __construct() {
        $dsn = 'mysql:host='.$this->host.';dbname='.$this->dbname;
        $this->db = new PDO($dsn,$this->user,$this->pass);
    }

    function getDatabase() {
        $this->db->query("SET NAMES utf8;");
        return $this->db;
    }
    
    function getDatabaseError() {
        $this->db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION ); 
        return $this->db;
    }
}