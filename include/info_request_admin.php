<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 22.04.2018
 * Time: 2:37
 */
include_once 'setting.php';

if($_SESSION['login'] == $login or $_SESSION['password'] == $password) {
//Проверка есть ли гет параметр в котором находится ID заявки которую обрабатываем
	if ( ! empty( $_GET['id'] ) ) {
		//Проверяем, нужно что бы тип всегда был числовым
		if ( ctype_digit( $_GET['id'] ) ) {
			//Переносим в переменную значение
			$id = $_GET['id'];
			//Соединяемся с базой
			spl_autoload_register( function ( $classname ) {
				require '../../' . $classname . '.php';
			} );
			$obj = new database();
			$pdo = $obj->getDatabase();
			$pdo = $obj->getDatabaseError();
			//Достаем данные по заявке
			$array_journal = $pdo->prepare( "SELECT * FROM `journal` WHERE id='$id'" );
			$array_journal->execute();
			$journal = $array_journal->fetch( PDO::FETCH_ASSOC );
			//формируем статус заявки
			if ( $journal['action'] == 2 ) {
				$status_journal = 'Подтвержден';
			} elseif ( $journal['action'] == 1 ) {
				$status_journal = 'Закрыта';
			} else {
				$status_journal = 'Ожидает обработки';
			}
			//Формируем дату заявки
			$date_journal = date( 'l, d F Y', $journal['time'] );

			//Достаем информацию о клиенте
			$array_client = $pdo->prepare( "SELECT * FROM `client` WHERE id='$journal[client_id]'" );
			$array_client->execute();
			$client = $array_client->fetch( PDO::FETCH_ASSOC );
			//Переносим данные в переменные
			$name_client  = $client['name'];
			$phone_client = $client['phone'];
			$date_client  = $client['date_of_birth'];

			//Достаем информацию о туре
			$array_tour = $pdo->prepare( "SELECT * FROM `tour` WHERE id='$journal[tour_id]'" );
			$array_tour->execute();
			$tour          = $array_tour->fetch( PDO::FETCH_ASSOC );
			$array_country = $pdo->prepare( "SELECT * FROM `country` WHERE id='$tour[country]'" );
			$array_country->execute();
			$country = $array_country->fetch( PDO::FETCH_ASSOC );
			//Переносим данные в переменны и формируем данные
			$name_tour        = $tour['name'];
			$country_tour     = $country['name'];
			$price_tour       = $tour['price'];
			$description_tour = $tour['description'];
			if ( $tour['action'] == 0 ) {
				$status_tour = 'Актуален';
			} else {
				$status_tour = 'Закрыт';
			}
			$images_tour = $tour['images'];
		}
	}
}