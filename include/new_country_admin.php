<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 23:22
 */
include_once 'setting.php';

if($_SESSION['login'] == $login or $_SESSION['password'] == $password) {
	//Соединяемся с базой
	spl_autoload_register( function ( $classname ) {
		require '../../' . $classname . '.php';
	} );
	$obj = new database();
	$pdo = $obj->getDatabase();
	$pdo = $obj->getDatabaseError();
	//Метка успешности добавления
	$new_count = false;
	//Узнаем пришли ли к нам данные.
	//Проверка на пустоту массива и существование кнопки сабмит
	if ( ! empty( $_POST ) and isset( $_POST['submit'] ) ) {

		//Проверяем что бы нам хватало заполненных данных
		if ( ! empty( $_POST['name'] ) and isset( $_FILES['image-file'] ) ) {
			$name_country = trim( $_POST['name'] );
			//Обрабатываем файл
			if ( ( $_FILES['image-file'] == "none" ) OR ( empty( $_FILES['image-file']['name'] ) ) ) {
				$message = "Вы не выбрали файл";
			} else if ( $_FILES['image-file']["size"] == 0 OR $_FILES['image-file']["size"] > 20050000 ) {
				$message = "Размер файла не соответствует нормам";
			} else if ( ( $_FILES['image-file']["type"] != "image/jpeg" ) AND ( $_FILES['image-file']["type"] != "image/jpeg" ) AND ( $_FILES['image-file']["type"] != "image/png" ) ) {
				$message = "Допускается загрузка только картинок JPG и PNG.";
			} else if ( ! is_uploaded_file( $_FILES['image-file']["tmp_name"] ) ) {
				$message = "Что-то пошло не так. Попытайтесь загрузить файл ещё раз.";
			} else {
				$name = rand( 1, 1000 ) . '-' . md5( $_FILES['image-file']['name'] ) . $_FILES['image-file']['name'];
				move_uploaded_file( $_FILES['image-file']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/img/country/" . $name );
				$size = @getimagesize( $_SERVER['DOCUMENT_ROOT'] . '/img/country/' . $name );
				if ( $size[0] < 50 OR $size[1] < 50 ) {
					unlink( $_SERVER['DOCUMENT_ROOT'] . '/img/country/' . $name );
					$message = "Файл не является допустимым изображением";
				} else {
					//Записываем в базу новую страну
					$array_country['name']   = $name_country;
					$array_country['images'] = $name;

					$insert_country = $pdo->prepare( "INSERT INTO `country` (name, images) values (:name, :images)" );
					$insert_country->execute( $array_country );
					$new_count = true;
					$message   = '';
				}
			}
		} else {
			$message = 'Название страны и картинка обязательна к заполнению';
		}

		//Проверка и выдача результата в зависимости от итога проверок
		if ( $new_count ) {
			echo '<div class="alert alert-success" role="alert">
				  <h4 class="alert-heading">Страна добавлена!</h4>
				  <p>Вы можете сразу приступать к добавлению новой страны</p>
				</div>';
		} else {
			echo '<div class="alert alert-danger" role="alert">
				  <h4 class="alert-heading">Ошибка добавления!</h4>
				  <p>' . $message . '</p>
				</div>';
		}
	}
}