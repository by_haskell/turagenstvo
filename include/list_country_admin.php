<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 0:20
 */

	//Соединяемся с базой
	spl_autoload_register(function ($classname) {
		require  '../../'.$classname . '.php';
	});

	$obj = new database();
	$pdo = $obj->getDatabase();
	$pdo = $obj->getDatabaseError();

	//Проверяем, есть ли вообще страны в базе
	$query_country = $pdo->query("SELECT COUNT(*) as count FROM `country`");
	$query_country->setFetchMode(PDO::FETCH_ASSOC);
	$count_country = $query_country->fetch();
	if ($count_country['count'] > 0) {
		//Формируем массив данных со странами мира которые есть в базе
		$array_country = $pdo->prepare("SELECT * FROM `country`");
		$array_country->execute();
		while($country = $array_country->fetch(PDO::FETCH_ASSOC)){
			echo '<tr class="gradeA odd" role="row">
							<td class="">'.$country['id'].'</td>
							<td class="sorting_1">'.$country['name'].'</td>
							<td>'.$country['images'].'</td>
							<td><a href="/img/country/'.$country['images'].'">
								<div class="text-center">
  									<img src="/img/country/'.$country['images'].'" class="rounded img-country" alt="'.$country['name'].'">
								</div></a>
							</td>
						</tr>';
		}
	}