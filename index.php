<?php include_once 'include/list_tour.php'; ?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Турагенство - Darkill</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="/css/product.css" rel="stylesheet">
</head>

<body>

<header>
    <div class="collapse bg-dark" id="navbarHeader">
        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-md-7 py-4">
                    <h4 class="text-white">Турагенство</h4>
                    <p class="muted-color text-muted">На нашем сайте вы найдете большой выбор интересных предложений. Путешествуйте с комфортом — отдыхайте с нами! Пляжные курорты или экзотические страны — каким будет отдых в этом году, решать Вам.</p>
                </div>
                <div class="col-sm-4 offset-md-1 py-4">
                    <h4 class="text-white">Наши контакты</h4>
                    <ul class="list-unstyled">
                        <li><a href="tel:+78949493929" class="text-white">+78949493929</a></li>
                        <li><a href="tel:+78949493930" class="text-white">+78949493930</a></li>
                        <li><a href="tel:+78949493931" class="text-white">+78949493931</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="navbar navbar-dark bg-dark box-shadow">
        <div class="container d-flex justify-content-between">
            <a href="#" class="navbar-brand d-flex align-items-center">
                <img class="d-block mx-auto mb-4" src="/img/globe.svg" alt="" width="30" height="30 align='center'">
                <strong>Darkill</strong>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarHeader" aria-controls="navbarHeader" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </div>
    </div>
</header>

<main role="main">

    <section class="jumbotron text-center img-top-agenstvo">
        <div class="container">
            <h1 class="jumbotron-heading">Турагенство - Darkill</h1>
            <p class="lead text-muted">Путешествуйте с комфортом - отдыхайте с нами!</p>
            <p>
                <a href="/country_of_rest.php" class="btn btn-primary my-2">Выбрать место отдыха</a>
            </p>
        </div>
    </section>

    <div class="album py-5 bg-light">
        <div class="container">

            <div class="row">
                <?=$html?>
            </div>
        </div>
    </div>

</main>

<footer class="text-muted">
    <div class="container">
        <p class="float-right">
            <a href="#">Подняться на верх</a>
        </p>
        <p>Турагенство - Darkill &copy; <?=date('Y')?></p>
    </div>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="js/vendor/popper.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/vendor/holder.min.js"></script>
</body>
</html>
