<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 0:34
 */
include_once 'setting.php';

if($_SESSION['login'] == $login or $_SESSION['password'] == $password) {
	//Соединяемся с базой
	spl_autoload_register( function ( $classname ) {
		require '../../' . $classname . '.php';
	} );

	$obj = new database();
	$pdo = $obj->getDatabase();
	$pdo = $obj->getDatabaseError();

	//Проверяем, есть ли вообще тур для этой страны в базе
	$query_client = $pdo->query( "SELECT COUNT(*) as count FROM `client`" );
	$query_client->setFetchMode( PDO::FETCH_ASSOC );
	$count_client = $query_client->fetch();
	if ( $count_client['count'] > 0 ) {

		//Формируем массив данных с турами страны которые есть в базе
		$array_client = $pdo->prepare( "SELECT * FROM `client`" );
		$array_client->execute();
		while ( $client = $array_client->fetch( PDO::FETCH_ASSOC ) ) {
			$array_journal = $pdo->prepare( "SELECT * FROM `journal` WHERE client_id='$client[id]'" );
			$array_journal->execute();
			$journal = $array_journal->fetch( PDO::FETCH_ASSOC );
			if ( ! empty( $journal['id'] ) ) {
				$array_tour = $pdo->prepare( "SELECT * FROM `tour` WHERE id='$journal[tour_id]'" );
				$array_tour->execute();
				$tour = $array_tour->fetch( PDO::FETCH_ASSOC );
			}

			echo '<tr class="gradeA odd" role="row">
							<td class="">' . $client['name'] . '</td>
							<td class="sorting_1">' . $client['phone'] . '</td>
							<td>' . $client['date_of_birth'] . '</td>
							<td class="center">';
			if ( ! empty( $journal['id'] ) ) {
				echo '<a href = "/admin/request/edit.php?id=' . $journal['id'] . '" >' . $tour['name'] . '</a >';
			}
			echo '</td>
						</tr>';
		}
	}
}
