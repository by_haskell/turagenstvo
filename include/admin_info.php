<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 21:27
 */
//Формируем данные для вывода на главной странцие админки

//Соединяемся с базой
spl_autoload_register(function ($classname) {
	require  '../'.$classname . '.php';
});

$obj = new database();
$pdo = $obj->getDatabase();
$pdo = $obj->getDatabaseError();
include_once 'setting.php';

if($_SESSION['login'] == $login or $_SESSION['password'] == $password) {

//Высчитываем

//количество клиентов
	$query_client = $pdo->query( "SELECT COUNT(*) as count FROM `client`" );
	$query_client->setFetchMode( PDO::FETCH_ASSOC );
	$count_client = $query_client->fetch();
	$client_count = $count_client['count'];

//Количество туров
	$query_tour = $pdo->query( "SELECT COUNT(*) as count FROM `tour`" );
	$query_tour->setFetchMode( PDO::FETCH_ASSOC );
	$count_tour = $query_tour->fetch();
	$tour_count = $count_tour['count'];

//Количество стран
	$query_country = $pdo->query( "SELECT COUNT(*) as count FROM `country`" );
	$query_country->setFetchMode( PDO::FETCH_ASSOC );
	$count_country = $query_country->fetch();
	$country_count = $count_country['count'];

//Количество заявок
	$query_journal = $pdo->query( "SELECT COUNT(*) as count FROM `journal`" );
	$query_journal->setFetchMode( PDO::FETCH_ASSOC );
	$count_journal = $query_journal->fetch();
	$journal_count = $count_journal['count'];
}