<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 0:20
 */

	//Соединяемся с базой
	spl_autoload_register(function ($classname) {
		require  $classname . '.php';
	});

	$obj = new database();
	$pdo = $obj->getDatabase();
	$pdo = $obj->getDatabaseError();

	//Проверяем сколько стран у нас для туров
	$query_country = $pdo->query("SELECT COUNT(*) as count FROM `country`");
	$query_country->setFetchMode(PDO::FETCH_ASSOC);
	$count_country = $query_country->fetch();
	//выводим количество
	echo $count_country['count'];