<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 23:22
 */
include_once 'setting.php';

if($_SESSION['login'] == $login or $_SESSION['password'] == $password) {
//Соединяемся с базой
	spl_autoload_register( function ( $classname ) {
		require '../../' . $classname . '.php';
	} );
	$obj = new database();
	$pdo = $obj->getDatabase();
	$pdo = $obj->getDatabaseError();
	//Метка успешности добавления
	$new_count = false;
	//Узнаем пришли ли к нам данные.
	//Проверка на пустоту массива и существование кнопки сабмит
	if ( ! empty( $_POST ) and isset( $_POST['submit'] ) ) {

		//Проверяем что бы нам хватало заполненных данных
		if ( ! empty( $_POST['name'] ) and ! empty( $_POST['description'] ) and ! empty( $_POST['country'] ) and ! empty( $_POST['price'] ) and isset( $_FILES['image-file'] ) ) {
			$name_tour        = trim( $_POST['name'] );
			$description_tour = trim( $_POST['description'] );
			$country_tour     = trim( $_POST['country'] );
			$price     = trim( $_POST['price'] );
			//Обрабатываем файл
			if ( ( $_FILES['image-file'] == "none" ) OR ( empty( $_FILES['image-file']['name'] ) ) ) {
				$message = "Вы не выбрали файл";
			} else if ( $_FILES['image-file']["size"] == 0 OR $_FILES['image-file']["size"] > 20050000 ) {
				$message = "Размер файла не соответствует нормам";
			} else if ( ( $_FILES['image-file']["type"] != "image/jpeg" ) AND ( $_FILES['image-file']["type"] != "image/jpeg" ) AND ( $_FILES['image-file']["type"] != "image/png" ) ) {
				$message = "Допускается загрузка только картинок JPG и PNG.";
			} else if ( ! is_uploaded_file( $_FILES['image-file']["tmp_name"] ) ) {
				$message = "Что-то пошло не так. Попытайтесь загрузить файл ещё раз.";
			} else {
				$name = rand( 1, 1000 ) . '-' . md5( $_FILES['image-file']['name'] ) . $_FILES['image-file']['name'];
				move_uploaded_file( $_FILES['image-file']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . "/img/tour/" . $name );
				$size = @getimagesize( $_SERVER['DOCUMENT_ROOT'] . '/img/tour/' . $name );
				if ( $size[0] < 50 OR $size[1] < 50 ) {
					unlink( $_SERVER['DOCUMENT_ROOT'] . '/img/tour/' . $name );
					$message = "Файл не является допустимым изображением";
				} else {
					//Записываем в базу новую страну
					$array_tour['name']        = $name_tour;
					$array_tour['description'] = $description_tour;
					$array_tour['country']     = $country_tour;
					$array_tour['images']      = $name;
					$array_tour['price']       = $price;
					$array_tour['time']        = time();

					$insert_tour = $pdo->prepare( "INSERT INTO `tour` (name, description, country, images, price, time) values (:name, :description, :country, :images, :price, :time)" );
					$insert_tour->execute( $array_tour );
					$new_count = true;
					$message   = '';
				}
			}
		} else {
			$message = 'Все поля обязательны к заполнению';
		}

		//Проверка и выдача результата в зависимости от итога проверок
		if ( $new_count ) {
			echo '<div class="alert alert-success" role="alert">
				  <h4 class="alert-heading">Тур добавлен!</h4>
				  <p>Вы можете сразу приступать к добавлению нового тура!</p>
				</div>';
		} else {
			echo '<div class="alert alert-danger" role="alert">
				  <h4 class="alert-heading">Ошибка добавления!</h4>
				  <p>' . $message . '</p>
				</div>';
		}
	}
}