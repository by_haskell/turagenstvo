<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 21.04.2018
 * Time: 0:34
 */
//Объявляем переменную в которой будет хранится весь html код выводимый на странице оформления
	$html = '';
	//Проверяем что бы к нам пришел не пустой массив данных. Проверка на пустой массив должна быть отрицательной
	if(!empty($_POST)){
		//Значение выбранной страны хранится в поле country.
		//Мы проверяем что бы оно не было пустым и числовым значение(ctype_degit).
		if(!empty($_POST['country']) and ctype_digit($_POST['country'])){
			//Заносим значение в переменнуб предварительно убрав возможные пробелы до и после числа
			$country = trim($_POST['country']);
			//Соединяемся с базой
			spl_autoload_register(function ($classname) {
				require  $classname . '.php';
			});

			$obj = new database();
			$pdo = $obj->getDatabase();
			$pdo = $obj->getDatabaseError();

			//Проверяем, есть ли вообще тур для этой страны в базе
			$query_tour = $pdo->query("SELECT COUNT(*) as count FROM `tour` WHERE country='$country'");
			$query_tour->setFetchMode(PDO::FETCH_ASSOC);
			$count_tour = $query_tour->fetch();
			if ($count_tour['count'] > 0) {
				//Начинаем формировать html контейнер в котором будут все туры по странам
				$html .= '<div class="card-group">';
				//Формируем массив данных с турами страны которые есть в базе
				$array_tour = $pdo->prepare("SELECT * FROM `tour` WHERE country='$country'");
				$array_tour->execute();
				while($tour = $array_tour->fetch(PDO::FETCH_ASSOC)){
					//Формируем тело html который выведется пользователю
					$html .= '<div class="card" id="'.$tour['id'].'">
				    <img class="card-img-top" src="/img/tour/'.$tour['images'].'" alt="'.$tour['name'].'">
				    <div class="card-body">
				      <h5 class="card-title">'.$tour['name'].'</h5>
				      <p class="card-text">'.$tour['description'].'</p>
				      <p class="card-text"><small class="text-muted lead">Стоимость: '.$tour['price'].' ₽</small></p>
				      <p class="card-text"><small class="text-muted">Размещено: '.date('l, d F Y', $tour['time']).'</small></p>
				    </div>
				  </div>';
				}
				//Закрываем контейнер
				$html .= '</div>';
			}else{
				$html .='<div class="alert alert-danger" role="alert">
				  <h4 class="alert-heading">Внимание!</h4>
				  <p>В выбранной Вами стране наше турагенство еще не предоставляет услуги.</p>
				  <hr>
				  <p class="mb-0">Вернуться к <a href="/country_of_rest.php">выбору страны</a>.</p>
				</div>';
			}
		}else{
			$html .='<div class="alert alert-danger" role="alert">
				  <h4 class="alert-heading">Внимание!</h4>
				  <p>В выбранной Вами стране наше турагенство еще не предоставляет услуги.</p>
				  <hr>
				  <p class="mb-0">Вернуться к <a href="/country_of_rest.php">выбору страны</a>.</p>
				</div>';
		}
	}else{
		$html .='<div class="alert alert-danger" role="alert">
				  <h4 class="alert-heading">Внимание!</h4>
				  <p>В выбранной Вами стране наше турагенство еще не предоставляет услуги.</p>
				  <hr>
				  <p class="mb-0">Вернуться к <a href="/country_of_rest.php">выбору страны</a>.</p>
				</div>';
	}