<?php
/**
 * Created by PhpStorm.
 * User: Home-PC
 * Date: 22.04.2018
 * Time: 3:06
 */
include_once 'setting.php';

if($_SESSION['login'] == $login or $_SESSION['password'] == $password) {
	//Соединяемся с базой
	spl_autoload_register( function ( $classname ) {
		require '../../' . $classname . '.php';
	} );
	$obj   = new database();
	$pdo   = $obj->getDatabase();
	$pdo   = $obj->getDatabaseError();
	$check = false;
	//Проверяем нужно ли обрабатывать форму, если да то заносим изменения
	//Если форму обрабатывать не нужно, то выводим блок для управления
	if ( isset( $_POST['submit'] ) ) {
		//Проверяем что бы выбор статуса заявки и его ID не были пустыми
		if ( ! empty( $_POST['request'] ) and ! empty( $_POST['request_id'] ) ) {
			//Проверяем верные ли значения к нам пришли
			if ( $_POST['request'] == 'no' or $_POST['request'] == 'yes' ) {
				//Проверяем числовое ли значение передано в качестве ID заявки
				if ( ctype_digit( $_POST['request_id'] ) ) {
					//Формулируем для вставки число статуса заявки
					if ( $_POST['request'] == 'no' ) {
						$action = 1;
					} else {
						$action = 2;
					}
					//Обновляем поле отвечающее за статус
					$update = $pdo->prepare( "UPDATE `journal` SET action='$action' WHERE id='$_POST[request_id]'" );
					$update->execute();
					$check = true;
				}
			}
		}
		//Проверяем каков ответ после проверок пришел
		if ( $check ) {
			//Заявка обработана, выдаем соответствующее сообщение
			echo '<div class="alert alert-success" role="alert">
				  <h4 class="alert-heading">Заявка обработана!</h4>
				  <p>Вы можете сразу приступать к <a href="/admin/request">другим заявкам</a></p>
				</div>';
		} else {
			//Так как заявка не обработана, мы выдаем обратно форму со значком ошибки
			echo '<div class="panel-footer">
                    <form method="post">
                        <div class="form-group not-margin-bottom">
                        <input type="hidden" name="request_id" value="' . $_GET['id'] . '">
                            <label class="radio-inline">
                                <input type="radio" name="request" id="optionsRadiosInline1" value="yes" checked="">Подтвердить
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="request" id="optionsRadiosInline2" value="no">Отклонить
                            </label>
                            <label class="radio-inline">
                                <input type="button" class="btn btn-danger btn-xs" value="Сохранить" name="submit">
                            </label>
                        </div>
                    </form>
                </div>';
		}
	} else {
		if ( ctype_digit( $_GET['id'] ) ) {
			$array_journal = $pdo->prepare( "SELECT action FROM `journal` WHERE id='$_GET[id]'" );
			$array_journal->execute();
			$journal = $array_journal->fetch( PDO::FETCH_ASSOC );
			if ( $journal['action'] == 0 ) {
				echo '<div class="panel-footer">
                    <form method="post">
                        <div class="form-group not-margin-bottom">
                        <input type="hidden" name="request_id" value="' . $_GET['id'] . '">
                            <label class="radio-inline">
                                <input type="radio" name="request" id="optionsRadiosInline1" value="yes" checked="">Подтвердить
                            </label>
                            <label class="radio-inline">
                                <input type="radio" name="request" id="optionsRadiosInline2" value="no">Отклонить
                            </label>
                            <label class="radio-inline">
                                <input type="submit" class="btn btn-primary btn-xs" value="Сохранить" name="submit">
                            </label>
                        </div>
                    </form>
                </div>';
			}
		}
	}
}